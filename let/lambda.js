let AWS = require('aws-sdk');
const sns = new AWS.SNS();

exports.handler = function (event, context, callback) {
    sns.publish({
        Message: 'Test msg1',
        Subject: 'Test sub1',
        MessageAttributes: {},
        MessageStructure: 'String',
        TopicArn: 'arn:aws:sns:us-east-1:318300609668:Test1'
    }).promise()
        .then(data => {
            // your code goes here this one is edited
        })
        .catch(err => {
            // error handling goes here
        });
    sns.publish({
        Message: 'Test msg 2',
        Subject: 'Test sub 2',
        MessageAttributes: {},
        MessageStructure: 'String',
        TopicArn: 'arn:aws:sns:us-east-1:318300609668:Test1'
    }).promise()
        .then(data => {
            // your code goes here
            callback(null, { "message": data });
        })
        .catch(err => {
            // error handling goes here
        });
    callback(null, { "message": "Success" });

}